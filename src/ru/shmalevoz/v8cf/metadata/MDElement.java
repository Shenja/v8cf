/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata;

import ru.shmalevoz.v8cf.metadata.parsers.EntryParser;

/**
 * Описание элемента объекта метаданных
 * @author shmalevoz
 */
public class MDElement {
	
	/**
	 * Конечные данные
	 */
	public static final int FINAL_DATA = 0;
	/**
	 * Вложенная секция
	 */
	public static final int SUBSECTION = 1;
	/**
	 * Модуль
	 */
	public static final int MODULE = 2;
	/**
	 * Форма
	 */
	public static final int FORM = 3;
	/**
	 * WS ссылка
	 */
	public static final int WSDL = 4;
	
	private final String name;
	private final String postfix;
	private final int type;
	private final EntryParser parser;
	
	public MDElement(String n, String s, int t) {
		this(n, s, t, null);
	}
	
	public MDElement(String n, String s, int t, EntryParser p) {
		name = n;
		postfix = s;
		type = t;
		parser = p;
	}
	
	/**
	 * Возвращает имя
	 * @return 
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Возвращает идентификатор элемента
	 * @return 
	 */
	public String getPostfix() {
		return postfix;
	}
	
	/**
	 * Возвращает тип элемента
	 * @return 
	 */
	public int getType() {
		return type;
	}
	
	/**
	 * Возвращает парсер эллемента, может быть null
	 * @return 
	 */
	public EntryParser getParser() {
		return parser;
	}
}
