/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers;

import ru.shmalevoz.v8cf.metadata.MDElement;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.MDEntryLink;
import ru.shmalevoz.v8cf.metadata.parsers.sections.Module;
import ru.shmalevoz.v8cf.storage.EntryLink;

/**
 * Базовый абстрактный парсер объектов метаданных
 * @author shmalevoz
 */
public abstract class AbstractParser implements EntryParser {
	
	private static final Logger log = ru.shmalevoz.utils.Log.getLogger(AbstractParser.class.getName());

	/**
	 * Возвращает наличие идентификатор секции
	 * @return 
	 */
	@Override
	public boolean hasSectionUUID() {
		return getSectionUUID() != null;
	}
	
	/**
	 * Возвращает идентификатор секции. Должен быть переопределен в потомках
	 * @return null
	 */
	@Override
	public String getSectionUUID() {
		return null;
	}

	/**
	 * Возвращает путь к имени объекта в разбираемой части словаря. Переопределяется в потомках
	 * @return Исключение
	 */
	@Override
	public String getObjectNamePath() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	/**
	 * Возвращает список ссылок на подчиненные объекты. Должен быть переопределен в потомках
	 * @param e Элемент родитель
	 * @return Список ссылок на подчиненные объекты
	 * @throws IOException 
	 */
	@Override
	public List<MDEntryLink> listEntriesLinks(MDEntry e) throws IOException {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
	//----------------
	
	/**
	 * Возвращает пустой список ссылок на подчиненные элементы
	 * @return 
	 */
	protected List<MDEntryLink> createMDEntryLinkList() {
		ArrayList<MDEntryLink> retval = new ArrayList<>();
		return retval;
	}
	
	/**
	 * Возвращает список возможных элементов объекта
	 * @return Список возможных элементов объекта
	 */
	@Override
	public List<MDElement> listElements() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}
	
	/**
	 * Проверяет наличие идентификатора в словаре
	 * @param dictionary Словарь метаданных
	 * @param uuid Искомый идентификатор
	 * @return Флаг наличия идентификатора
	 */
	private boolean containsUUID(Tree<EntryLink> dictionary, String uuid) {
		return dictionary.get(uuid, true) != null;
	}
	
	/**
	 * Добавляет ссылку на модуль
	 * @param list Результатирующий список
	 * @param name Имя модуля
	 * @param uuid Идентификатор в хранилище
	 * @param parent Элемент родитель
	 * @param dictionary Словарь ссылок хранилища
	 */
	protected void addEntryLinkModule(List<MDEntryLink> list, String name, String uuid, MDEntry parent, Tree<EntryLink> dictionary) {
		
		if (containsUUID(dictionary, uuid)) { // Если в хранилище есть элемент с таким идентификатором
			list.add(new MDEntryLink(name, new Module(uuid), parent));
		}
	}
	
	/**
	 * Добавляет ссылку на модуль
	 * @param list
	 * @param name
	 * @param uuid
	 * @param parent
	 * @param dictionary 
	 * @throws java.io.IOException 
	 */
	protected void addEntryLinkForm(List<MDEntryLink> list, String name, String uuid, MDEntry parent, Tree<EntryLink> dictionary) throws IOException {
		
		Tree<EntryLink> leaf = dictionary.get(uuid, true);
		if (leaf != null) {
			
			if (parent.getStorageEntryReader().read(leaf.getValue()).isContainer()) { // Обычная форма
				
				list.add(new MDEntryLink(
					"Форма"
					, new EmptyParser()
					, parent
					, leaf.get("form", true).getValue()
					, false));
				list.add(new MDEntryLink(
					"Модуль"
					, new EmptyParser()
					, parent
					, leaf.get("module", true).getValue()
					, false));
			} else { // Управляемая форма. Все данные в одном элементе хранилища
				list.add(new MDEntryLink(
					"Форма"
					, new EmptyParser()
					, parent
					, leaf.getValue()
					, false));
			}
			
		}
	}
	
	/**
	 * Добавляет ссылку на данные WS ссылки
	 * @param list
	 * @param name
	 * @param uuid
	 * @param parent
	 * @param dictionary
	 */
	protected void addEntryLinkWSDL(List<MDEntryLink> list, String name, String uuid, MDEntry parent, Tree<EntryLink> dictionary) {
		
		Tree<EntryLink> leaf = dictionary.get(uuid, true);
		
		if (leaf != null) {
			list.add(new MDEntryLink(
				name
				, new EmptyParser()
				, parent
				, leaf.get("0.wsdl", true).getValue()
				, false));
		}
		
	}
	
	/**
	 * Добавляет в список подчиненных ссылку на подчиненную секцию, если идентификатор секции есть в словаре
	 * @param l Изменяемый список элементов
	 * @param n Имя секции
	 * @param e Парсер секции
	 * @param p Элемент родитель
	 */
	protected void addEntryLinkSubsection(List<MDEntryLink> l, String n, EntryParser e, MDEntry p) {
		addEntryLinkSubsection(l, n, e, p, p.getDictionary());
	}
	
	/**
	 * Добавляет в список подчиненных ссылку на подчиненную секцию, если идентификатор секции есть в словаре
	 * @param l Изменяемый список элементов
	 * @param n Имя секции
	 * @param e Парсер секции
	 * @param p Элемент родитель
	 * @param d Словарь идентификаторов метаданных
	 */
	protected void addEntryLinkSubsection(List<MDEntryLink> l, String n, EntryParser e, MDEntry p, Tree<String> d) {
		if (!hasSectionUUID() || d.get(e.getSectionUUID(), true) != null) {
			l.add(new MDEntryLink(n, e, p));
		}
	}
	
	/**
	 * Добавляет в список подчиеннных ссылку на подчиеннный элемент
	 * @param retval Изменяемый список
	 * @param n Имя элемента
	 * @param e Парсер элемента
	 * @param p Родитель элемента
	 * @param l Ссылка на данные элемента в хранилище
	 */
	protected void addEntryLinkElement(List<MDEntryLink> retval, String n, EntryParser e, MDEntry p, EntryLink l) {
		retval.add(new MDEntryLink(n, e, p, l, false));
	}
	
	/**
	 * Добавляет в список подчиеннных ссылку на подчиеннный элемент
	 * @param retval Изменяемый список
	 * @param n Имя элемента
	 * @param e Парсер элемента
	 * @param p Родитель элемента
	 * @param ln Имя ссылки в дереве
	 * @param tl Дерево ссылок на элементы хранилища
	 */
	protected void addEntryLinkElement(List<MDEntryLink> retval, String n, EntryParser e, MDEntry p, String ln, Tree<EntryLink> tl) {
		
		Tree<EntryLink> t = tl.get(ln, true);
		if (t != null) {
			addEntryLinkElement(retval, n, e, p, t.getValue());
		}
		
	}
}
