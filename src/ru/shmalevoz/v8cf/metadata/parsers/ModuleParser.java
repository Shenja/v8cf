/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers;

import java.io.IOException;
import java.util.List;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.MDEntryLink;
import ru.shmalevoz.v8cf.storage.Entry;
import ru.shmalevoz.v8cf.storage.EntryLink;

/**
 *
 * @author shmalevoz
 */
public abstract class ModuleParser extends SectionParser {
	
	@Override
	public List<MDEntryLink> listEntriesLinks(MDEntry e) throws IOException {
		
		List<MDEntryLink> retval = super.createMDEntryLinkList();
		
		// Модуль имеет 2 подчиненных элемента - info и text
		Tree<EntryLink> root = e.getStorageLinks().get(getSectionUUID(), true);
		EmptyParser p = new EmptyParser();
		
		// Здесь надо проверить, является ли объект хранилища контейнером
		// Почему-то некоторые содули идут не подконтейнерами, а обычными объектами хранилища
		Entry s = e.getStorageEntryReader().read(root.getValue());
		
		if (s.isContainer()) {
			retval.add(new MDEntryLink("Информация", p, e, root.get("info", true).getValue(), false));
			retval.add(new MDEntryLink("Текст", p, e, root.get("text", true).getValue(), false));
		} else {
			retval.add(new MDEntryLink("Текст", p, e, root.getValue(), false));
		}
		
		return retval;
	}
	
}
