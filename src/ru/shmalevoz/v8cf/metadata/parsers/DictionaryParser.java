/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.storage.Entry;
import ru.shmalevoz.v8cf.storage.EntryLink;
import ru.shmalevoz.v8cf.storage.Page;

/**
 * Работа со словарем метаданных
 * @author shmalevoz
 */
public class DictionaryParser {
	
	private static final Logger log = ru.shmalevoz.utils.Log.getLogger(DictionaryParser.class.getName());
	
	private static void addNode(Tree<String> p, String v) {
		if (!v.isEmpty()) {
			p.add(new Tree<>(v, v));
		}
	}
	
	/**
	 * Заполняет дерево данными словаря
	 * @param node Текущий лист дерева
	 * @param r Поток чтения словаря
	 */
	private static void fillTree(Tree<String> node, InputStreamReader r) throws IOException {
		
		StringBuilder buf = new StringBuilder();
		char[] ch = new char[1];
		String sim;
		boolean is_delim;
		
		while (r.read(ch) != -1) {
			
			if (ch[0] < ' ') continue; // Очищаем от управляющих символов
			
			sim = String.valueOf(ch);
			is_delim = false;
			
			// блок закончился, прерываем заполнение элемента
			if ("}".equals(sim)) {
				addNode(node, buf.toString());
				break;
			} else if (",".equals(sim)) {
				// Разделитель элементов
				is_delim = true;
				addNode(node, buf.toString());
			} else if ("{".equals(sim)) {
				is_delim = true;
				// Для корневого элемента первое открытие блока пропускаем
				if (!node.isRoot() || node.hasLeafs()) {
					// Новый подчиненный блок
					addNode(node, buf.toString());
				}
				Tree<String> l = new Tree<>();
				fillTree(l, r);
				node.add(l);
			}
			
			if (is_delim) {
				buf.delete(0, buf.length());
			} else {
				buf.append(ch);
			}
		}
	}
	
	/**
	 * Возвращает древовидное представление словаря метаданных
	 * @param s Поток чтения словаря
	 * @return Древовидное представление словаря
	 * @throws IOException При ошибке создания потока в кодировке UTF-8
	 */
	public static Tree<String> parse(InputStream s) throws IOException {
		
		InputStreamReader r;
		
		try {
			r = new InputStreamReader(s, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new UnsupportedOperationException(e.getMessage());
		}
		
		Tree<String> rootnode = new Tree<>();
		fillTree(rootnode, r);
		
		return rootnode;
	}
	
	/**
	 * Возвращает древовидное представление словаря метаданных
	 * @param s Словарь метаданных строкой
	 * @return Древовидное представление словаря метаданных
	 * @throws IOException В случае ошибки создания потока чтения в кодировке UTF-8
	 */
	public static Tree<String> parse(String s) throws IOException {
		return parse(new ByteArrayInputStream(s.getBytes()));
	}
	
	/**
	 * Возвращает древовидное представление словаря метаданных
	 * @param e Элемент хранилища конфигурации
	 * @return Древовидное представление словаря метаданных
	 * @throws IOException 
	 */
	public static Tree<String> parse(Entry e) throws IOException {
		return parse(e.getDataInputStream());
	}
	
	/**
	 * Возвращает словарь из элемента хранилища
	 * @param entry_name Имя элемента хранилища
	 * @param p Станица хранилища
	 * @param links Дерево ссылок хранилища
	 * @return Древовидное представление словаря метаданных
	 * @throws java.io.IOException
	 */
	public static Tree<String> parse(String entry_name, Page p, Tree<EntryLink> links) throws IOException {
		
		EntryLink l = links.get(entry_name, true).getValue();
		
		if (l == null) {
			throw new IOException("Не удалось получить ссылку на элемент с именем " + entry_name);
		}
		
		return DictionaryParser.parse(new Entry(p, l).getDataInputStream());
	}
	
	/**
	 * Возвращает элемент словаря
	 * @param dict Словарь
	 * @param path Путь к элементу
	 * @return Строка значения элемента, null если не найдено
	 */
	public static String getElement(Tree<String> dict, String path) {
		return getElement(dict, path.split("/"));
	}
	
	/**
	 * Возвращает элемент словаря
	 * @param dict Словарь
	 * @param path Путь к элементу
	 * @return Строка значения злемета, null если не найдено
	 */
	public static String getElement(Tree<String> dict, String[] path) {
		
		Tree<String> node = dict;
		
		for (int node_index = 1; node_index < path.length; node_index++) {
			node = node.getLeafAt(Integer.parseInt(path[node_index]));
			if (node == null) {
				return null;
			}
		}
		
		return node.getValue();
	}
}
