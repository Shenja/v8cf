/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class CalculationRegisters extends SectionParser {
	
	//private static final String name = "РегистрыРасчета";
	// b12fc850-8210-43c8-ae05-89567e698fbb Измерения
	// 702b33ad-843e-41aa-8064-112cd38cc92c Ресурсы
	// 1b304502-2216-440b-960f-60decd04bb5d Реквизиты
	
	private static final String UUID = "f2de87a8-64e5-45eb-a22d-b3aedab050e7";
	private static final String PATH = "0/0/1/15/1/2";
	private static final String FORMS_UUID = "a2cb086c-db98-43e4-a1a9-0760ab048f8d"; // 0/0/7/0
	private static final String COMMANDS_UUID = "acdf0f11-2d59-4e37-9945-c6721871a8fe"; // Путь 0/0/8/0
	private static final String COMMANDS_NAME_PATH = "0/0/1/2/9/2";
	private static final String COMMANDS_UUID_PATH = "0/0/1/1/1";
	

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".0", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("МодульНабораЗаписей", ".1", MDElement.MODULE));
		ELEMENTS.add(new MDElement("МодульМенеджера", ".2", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Перерасчеты", "", MDElement.SUBSECTION, new Recalculation()));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID,COMMANDS_NAME_PATH, COMMANDS_UUID_PATH)));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
