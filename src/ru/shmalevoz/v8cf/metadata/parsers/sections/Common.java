/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.List;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.MDEntryLink;
import ru.shmalevoz.v8cf.metadata.parsers.AbstractParser;

/**
 *
 * @author shmalevoz
 */
public class Common extends AbstractParser {

	@Override
	public List<MDEntryLink> listEntriesLinks(MDEntry e) {
		
		List<MDEntryLink> retval = super.createMDEntryLinkList();
		Tree<String> dict = e.getDictionary();
		
		super.addEntryLinkSubsection(retval, "Подсистемы", new Sybsystems(), e, dict);
		super.addEntryLinkSubsection(retval, "ОбщиеМодули", new CommonModules(), e, dict);
		super.addEntryLinkSubsection(retval, "ПараметрыСеанса", new SessionParameters(), e, dict);
		super.addEntryLinkSubsection(retval, "Роли", new Roles(), e, dict);
		super.addEntryLinkSubsection(retval, "ОбщиеРеквизиты", new CommonAttributes(), e, dict);
		super.addEntryLinkSubsection(retval, "ПланыОбмена", new ExchangePlans(), e, dict);
		super.addEntryLinkSubsection(retval, "КритерииОтбора", new Filter(), e, dict);
		super.addEntryLinkSubsection(retval, "ПодпискиНаСобытия", new EventSubscription(), e, dict);
		super.addEntryLinkSubsection(retval, "РегламентныеЗадания", new ScheduledJobs(), e, dict);
		super.addEntryLinkSubsection(retval, "Интерфейсы", new Interfaces(), e, dict);
		super.addEntryLinkSubsection(retval, "ФункциональныеОпции", new FunctionalOptions(), e, dict);
		super.addEntryLinkSubsection(retval, "ПараметрыФункциональныхОпций", new FunctionalOptionParameters(), e, dict);
		super.addEntryLinkSubsection(retval, "ОпределяемыеТипы", new DefinedTypes(), e, dict);
		super.addEntryLinkSubsection(retval, "ХранилищаНастроек", new SettingRepositories(), e, dict);
		super.addEntryLinkSubsection(retval, "ОбщиеФормы", new CommonForms(), e, dict);
		super.addEntryLinkSubsection(retval, "ОбщиеКоманды", new CommonCommands(), e, dict);
		super.addEntryLinkSubsection(retval, "ГруппыКоманд", new CommandGroups(), e, dict);
		super.addEntryLinkSubsection(retval, "ОбщиеМакеты", new CommonTemplates(), e, dict);
		super.addEntryLinkSubsection(retval, "Общиекартинки", new CommonPictures(), e, dict);
		super.addEntryLinkSubsection(retval, "XDTOПакеты", new XDTOPackages(), e, dict);
		super.addEntryLinkSubsection(retval, "WebСервисы", new WebServices(), e, dict);
		super.addEntryLinkSubsection(retval, "WSСсылки", new WSReferences(), e, dict);
		super.addEntryLinkSubsection(retval, "ЭлементыСтиля", new StyleElements(), e, dict);
		super.addEntryLinkSubsection(retval, "Стили", new Styles(), e, dict);
		super.addEntryLinkSubsection(retval, "Языки", new Languages(), e, dict);
		
		return retval;
	}
}
