/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.io.IOException;
import java.util.List;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.MDEntryLink;
import ru.shmalevoz.v8cf.metadata.parsers.AbstractParser;
import ru.shmalevoz.v8cf.metadata.parsers.DictionaryParser;
import ru.shmalevoz.v8cf.metadata.parsers.EmptyParser;
import ru.shmalevoz.v8cf.storage.EntryLink;

/**
 * Парсер служебных свойств внешней обработки
 * @author shmalevoz@gmail.com (Valeriy Krynin)
 */
public class ExternalDataProcessorProperties extends AbstractParser {
	
	@Override
	public List<MDEntryLink> listEntriesLinks(MDEntry e) {
		
		List<MDEntryLink> retval = super.createMDEntryLinkList();
		
		Tree<EntryLink> links = e.getStorageLinks();
		EmptyParser parser = new EmptyParser();
		
		// Служебные структуры
		super.addEntryLinkElement(retval, "root", parser, e, links.get("root", true).getValue());
		super.addEntryLinkElement(retval, "version", parser, e, links.get("version", true).getValue());
		super.addEntryLinkElement(retval, "versions", parser, e, links.get("versions", true).getValue());
		super.addEntryLinkElement(retval, "copyinfo", parser, e, links.get("copyinfo", true).getValue());
		
		// Словарь данных
		try {
			Tree<String> root_dict = DictionaryParser.parse("root", e.getStoragePage(), links);
			super.addEntryLinkElement(retval, "dictionary", parser, e
				, links.get(DictionaryParser.getElement(root_dict, "0/0/1"), true).getValue());
		} catch (IOException ex) {
			throw new UnsupportedOperationException("Не удалось получить элемент словаря конфигурации!");
		}
		
		return retval;
	}
}
