/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class ChartsOfCalculationTypes extends SectionParser {
	
	//private static final String name = "ПланыВидовРасчета";
	// 0dc22ad2-476a-4794-afae-cfa7ed251752 Реквизиты
	// 054aa8cf-faa6-4634-aef4-1087ca0d88fc Табличные части
	
	private static final String UUID = "30b100d6-b29f-47ac-aec7-cb8ca8a54767";
	private static final String PATH = "0/0/1/1/1/2";
	private static final String FORMS_UUID = "a7f8f92a-7a4b-484b-937e-42d242e64144"; // 0/0/7/0
	private static final String COMMANDS_UUID = "2e90c75b-2f0c-4899-a7d4-5426eaefc96e"; // Путь 0/0/5/0

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".1", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("ПредопределенныеЭлементы", ".2", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("МодульОбъекта", ".0", MDElement.MODULE));
		ELEMENTS.add(new MDElement("МодульМенеджера", ".3", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID)));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
