/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import ru.shmalevoz.v8cf.metadata.parsers.sections.Commands;
import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class ExchangePlans extends SectionParser {
	
	//private static final String name = "ПланыОбмена";
	private static final String UUID = "857c4a91-e5f4-4fac-86ec-787626f1c108";
	private static final String PATH = "0/0/1/12/2";
	private static final String FORMS_UUID = "87c509ab-3d38-4d67-b379-aca796298578"; // Путь к ид 0/0/6/0
	private static final String COMMANDS_UUID = "d5207c64-11d5-4d46-bba2-55b7b07ff4eb"; // Путь к ид 0/0/7/0

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".0", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("РаспределеннаяИнформационнаяБаза", ".1", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("МодульОбъекта", ".2", MDElement.MODULE));
		ELEMENTS.add(new MDElement("МодульМенеджера", ".3", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID)));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
