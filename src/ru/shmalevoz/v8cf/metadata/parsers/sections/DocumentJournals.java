/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import ru.shmalevoz.v8cf.metadata.parsers.sections.Commands;
import ru.shmalevoz.v8cf.metadata.parsers.sections.Columns;
import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class DocumentJournals extends SectionParser {
	
	//private static final String name = "ЖурналыДокументов";
	private static final String UUID = "4612bd75-71b7-4a5c-8cc5-2b0b65f9fa0d";
	private static final String PATH = "0/0/1/3/1/2";
	private static final String FORMS_UUID = "ec81ad10-ca07-11d5-b9a5-0050bae0a95d"; // Путь 0/0/6/0
	private static final String COMMANDS_UUID = "a49a35ce-120a-4c80-8eea-b0618479cd70"; // Путь 0/0/5/0

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".0", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("МодульМенеджера", ".1", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Графы", "", MDElement.SUBSECTION, new Columns()));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID, "0/0/1/2/9/2", "0/0/1/1/1")));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
