/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.MDEntryLink;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 * Парсер ветки Документы
 * @author shmalevoz
 */
public class Documents extends SectionParser {
	
	//private static final String name = "Документы";
	private static final String UUID = "061d872a-5787-460e-95ac-ed74ea3a3e84";
	private static final String PATH = "0/0/1/9/1/2";
	private static final String FORMS_UUID = "fb880e93-47d7-4127-9357-a20e69c17545"; // Путь 0/0/7/0
	private static final String COMMANDS_UUID = "b544fc6a-2ba3-4885-8fb2-cb289fb6d65e"; // Путь 0/0/6/0

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".1", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("МодульОбъекта", ".0", MDElement.MODULE));
		ELEMENTS.add(new MDElement("МодульМенеджера", ".2", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID)));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
	
	@Override
	public List<MDEntryLink> listEntriesLinks(MDEntry e) throws IOException {
		List<MDEntryLink> retval = super.listEntriesLinks(e);
		Tree<String> dictionary = e.getDictionary();
		
		super.addEntryLinkSubsection(retval, "Нумераторы", new DocumentNumerators(), e, dictionary);
		super.addEntryLinkSubsection(retval, "Последовательности", new Sequences(), e, dictionary);
		
		return retval;
	}
}
