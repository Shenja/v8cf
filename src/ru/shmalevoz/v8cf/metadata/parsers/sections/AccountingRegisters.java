/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class AccountingRegisters extends SectionParser {
	
	//private static final String name = "РегистрыБухгалтерии";
	private static final String UUID = "2deed9b8-0056-4ffe-a473-c20a6c32a0bc";
	private static final String PATH = "0/0/1/15/1/2";
	private static final String FORMS_UUID = "d3b5d6eb-4ea2-4610-a3e2-624d4e815934"; // 0/0/8/0
	private static final String FORMS_NAME_PATH = "0/0/1/1/2";
	private static final String COMMANDS_UUID = "7162da60-f7fe-4d78-ad5d-e31700f9af18"; // Путь 0/0/6/0
	private static final String COMMANDS_NAME_PATH = "0/0/1/2/9/2";
	private static final String COMMANDS_UUID_PATH = "0/0/1/1/1";
	

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".5", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("МодульНабораЗаписей", ".6", MDElement.MODULE));
		ELEMENTS.add(new MDElement("МодульМенеджера", ".7", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID, FORMS_NAME_PATH)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID,COMMANDS_NAME_PATH, COMMANDS_UUID_PATH)));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
