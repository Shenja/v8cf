/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.List;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.MDEntryLink;
import ru.shmalevoz.v8cf.metadata.parsers.AbstractParser;
import ru.shmalevoz.v8cf.metadata.parsers.DictionaryParser;
import ru.shmalevoz.v8cf.metadata.parsers.EmptyParser;
import ru.shmalevoz.v8cf.storage.EntryLink;

/**
 * Парсер внешней обработки
 * @author shmalevoz@gmail.com (Valeriy Krynin)
 */
public class ExternalDataProcessor extends AbstractParser {
	
	// 7e7123e0-29e2-11d6-a3c7-0050bae0a776 Реквизиты
	// b077d780-29e2-11d6-a3c7-0050bae0a776 Табличные части
	
	private static final String FORMS_UUID = "d5b0e5ed-256d-401c-9c36-f630cafd8a62"; // Путь 0/0/3/1/5/0
	private static final String FORMS_NAME_PATH = "0/0/1/1/1/1/2"; 

	@Override
	public List<MDEntryLink> listEntriesLinks(MDEntry e) {
		
		// ec6bb5e5-b7a8-4d75-bec9-658107a699cf Реквизиты
		// 2bcef0d1-0981-11d6-b9b8-0050bae0a95d Табличные части
		
		List<MDEntryLink> retval = super.createMDEntryLinkList();
		Tree<String> dict = e.getDictionary();
		
		Tree<EntryLink> links = e.getStorageLinks();
		EmptyParser parser = new EmptyParser();
		
		// Чтобы было видно запишем имя отчета. В качестве содержания поместим туда copyinfo
		String report_name = DictionaryParser.getElement(dict, "0/0/3/1/1/3/1/2");
		String report_uuid = DictionaryParser.getElement(dict, "0/0/3/1/1/3/1/1/2");
		super.addEntryLinkElement(retval, report_name, parser, e, links.get("copyinfo", true).getValue());
		
		super.addEntryLinkModule(retval, "МодульОбъекта", report_uuid.concat(".0"), e, links);
		super.addEntryLinkElement(retval, "СправочнаяИнформация", parser, e, report_uuid.concat(".1"), links);
		super.addEntryLinkSubsection(retval, "Формы", new Forms(FORMS_UUID, FORMS_NAME_PATH, dict), e, dict);
		super.addEntryLinkSubsection(retval, "Макеты", new Templates(dict), e, dict);
		
		super.addEntryLinkSubsection(retval, "Свойства", new ExternalReportProperties(), e);
		
		return retval;
	}
}
