/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class ChartsOfCharacteristicTypes extends SectionParser {
	
	//private static final String name = "ПланыВидовХарактеристик";
	// 31182525-9346-4595-81f8-6f91a72ebe06 Реквизиты
	// 54e36536-7863-42fd-bea3-c5edd3122fdc Табличные части
	
	private static final String UUID = "82a1b659-b220-4d94-a9bd-14d757b95a48";
	private static final String PATH = "0/0/1/13/1/2";
	private static final String FORMS_UUID = "eb2b78a8-40a6-4b7e-b1b3-6ca9966cbc94"; // 0/0/7/0
	private static final String FORMS_NAME_PATH = "0/0/1/1/2";
	private static final String COMMANDS_UUID = "95b5e1d4-abfa-4a16-818d-a5b07b7d3f73"; // Путь 0/0/6/0

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".5", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("ПредопределенныеЭлементы", ".7", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("МодульОбъекта", ".15", MDElement.MODULE));
		ELEMENTS.add(new MDElement("МодульМенеджера", ".16", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID, FORMS_NAME_PATH)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID)));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
