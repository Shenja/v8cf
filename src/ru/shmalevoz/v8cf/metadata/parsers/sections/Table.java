/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import ru.shmalevoz.v8cf.metadata.parsers.sections.Forms;
import ru.shmalevoz.v8cf.metadata.parsers.sections.Commands;
import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SubsectionParser;

/**
 *
 * @author shmalevoz
 */
public class Table extends SubsectionParser {

	//private static final String name = "Таблица"; // Таблица внешнего источника данных
	// c7075688-01c1-41ba-bd1c-1023c8189079 Поля таблицы
	
	private final String UUID = "e3403acd-1c95-421b-87e4-4dfa29d38b52";
	private final String PATH = "0/0/1/1/1/2";
	private static final String FORMS_UUID = "17816ebc-4068-496e-adc4-8879945a832f"; // 0/0/6/0
	private static final String FORMS_NAME_PATH = "0/0/1/1/2";
	private static final String COMMANDS_UUID = "5bb6f09e-5d80-41f6-8070-9faa4d15b69b"; // Путь 0/0/3/0
	
	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".0", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("МодульМенеджера", ".1", MDElement.MODULE));
		ELEMENTS.add(new MDElement("МодульОбъекта", ".2", MDElement.MODULE));
		ELEMENTS.add(new MDElement("МодульНабораЗаписей", ".3", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID, FORMS_NAME_PATH)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID)));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}
	
	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
