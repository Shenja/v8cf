/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.MDEntryLink;
import ru.shmalevoz.v8cf.metadata.parsers.SubsectionParser;

/**
 *
 * @author shmalevoz
 */
public class Templates extends SubsectionParser {
	
	// Макеты
	private static final String UUID = "3daea016-69b7-4ed4-9453-127911372fe6";
	private static final String PATH = "0/0/1/2/2";
	
	private final Tree<String> dictionary;

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("Данные", ".0", MDElement.FINAL_DATA));
	}

	public Templates() {
		this(null);
	}
	
	public Templates(Tree<String> d) {
		dictionary = d;
	}
	
	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
	
	@Override
	public List<MDEntryLink> listEntriesLinks(MDEntry e) throws IOException {
		if (dictionary == null) {
			return super.listEntriesLinks(e);
		}
		return super.listEntriesLinksFromDict(e, dictionary);
	}
	
}
