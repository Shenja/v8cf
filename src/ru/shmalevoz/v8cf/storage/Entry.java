/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.storage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Logger;
import java.util.Arrays;
import java.util.zip.Inflater;
import ru.shmalevoz.v8cf.IO.ByteArrayDataReader;
import ru.shmalevoz.v8cf.IO.RandomAccessible;

/**
 * Запись файла cf
 * 
 * @author shmalevoz
 */
public class Entry {
	
	private static final Logger log = ru.shmalevoz.utils.Log.getLogger(Entry.class.getName());
	
	private int attrs_rec_offset;
	private int data_rec_offset;
	private String name;
	private byte[] creationTime = new byte[8];
	private byte[] lastAccessTime = new byte[8];
	private byte[] data;
	private Page owner;
	
	/**
	 * Чтение атрибутов элемента.
	 * Структура 
	 * byte[8] время создания 
	 * byte[8] время последнего изменения
	 * byte[4] зарезервировано
	 * UTF16 строка UUID
	 * 
	 */
	private void readAttributes() throws IOException {
		
		Record rec = new Record(getSource(), attrs_rec_offset);
		
		creationTime = Arrays.copyOfRange(rec.getBody(), 0, 8);
		lastAccessTime = Arrays.copyOfRange(rec.getBody(), 8, 16);
		
		try {
			name = new String(Arrays.copyOfRange(rec.getBody(), 19, rec.getBody().length - 5), "UTF-16").trim().toLowerCase();
		} catch (Exception e) {
			String err = "Ошибка чтения атрибутов элемента " + Integer.toHexString(rec.getAddr());
			log.severe(err);
			throw new IOException(err);
		}
	}
	
	/**
	 * Возвращает данные элемента обходом цепочки записей
	 * 
	 * @return 
	 */
	private byte[] getEntryData() throws IOException {
		
		Record rec = new Record(getSource(), data_rec_offset);
		
		ByteArrayOutputStream o = new ByteArrayOutputStream();
		o.write(rec.getBody());
		
		while (rec.hasNext()) {
			rec.next();
			o.write(rec.getBody());
		}
		o.close();
		
		return o.toByteArray();
	}
	
	/**
	 * Чтение данныех элемента
	 */
	private void readData() throws IOException {
		
		if (getOwnerPage().getSource().containsCompressedData()) {
			
			Inflater unpack = new Inflater(true);
			unpack.setInput(getEntryData());
			
			ByteArrayOutputStream o = new ByteArrayOutputStream();
			byte[] buf = new byte[1024]; // Необходим большой буфер для распаковки, иначе падаем в OutOfMemory из-за новых копий буфера
			int len = 0;

			while (!unpack.finished()) {
				try {
					len = unpack.inflate(buf);
				} catch (Exception e) {
					throw new IOException(e.getMessage());
				}
				
				o.write(buf, 0, len);
			}
			unpack.end();
			o.close();
			data = o.toByteArray();
			
		} else { // instanceof FileDataInput
			
			data = getEntryData();
		}
	}
	
	/**
	 * Конструктор
	 * @param p Источник данных
	 * @param l Ссылка на элемент
	 * @throws IOException 
	 */
	public Entry(Page p, EntryLink l) throws IOException {
		this(p, l.getAttrAddr(), l.getDataAddr());
	}
	
	/**
	 *  Конструктор
	 * @param p Источник данных
	 * @param a_offset Смещение записи атрибутов
	 * @param d_offset Смещение записи данных
	 * @throws java.io.IOException 
	 */
	public Entry(Page p, int a_offset, int d_offset) throws IOException {
		
		owner = p;
		attrs_rec_offset = a_offset;
		data_rec_offset = d_offset;
		
		readAttributes();
		readData();
	}
	
	/**
	 * Возвращает имя элемента
	 * 
	 * @return 
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Возвращает данные элемента
	 * 
	 * @return 
	 */
	public byte[] getData() {
		return data;
	}
	
	/**
	 * Проверяет, является ли содержимое элемента контейнером
	 * @return 
	 */
	public boolean isContainer() {
		return ru.shmalevoz.v8cf.storage.Container.hasContainerHeader(data);
	}
	
	/**
	 * Возвращает страницу владельца элемента
	 * @return 
	 */
	public Page getOwnerPage() {
		return owner;
	}
	
	/**
	 * Возвращает полное имя элемента
	 * @param e
	 * @return 
	 */
	private String getEntryFullName(Entry e) {
		StringBuilder retval =  new StringBuilder();
		
		Entry p = e.getOwnerPage().getOwnerContainer().getOwnerEntry();
		if (p != null) {
			retval.append(getEntryFullName(p)).append("/");
		}
		
		retval.append(e.getName());
		
		return retval.toString();
	}
	
	/**
	 * Возвращает полное имя элемента
	 * @return 
	 */
	public String getFullName() {
		return getEntryFullName(this);
	}
	
	/**
	 * Возвращает источник данных
	 * @return 
	 */
	public RandomAccessible getSource() {
		return getOwnerPage().getSource();
	}
	
	/**
	 * Возвращает ссылку на элемент
	 * @return 
	 */
	public EntryLink getLink() {
		return new EntryLink(attrs_rec_offset, data_rec_offset
			, getOwnerPage().getOwnerContainer().getOwnerEntryLink());
	}
	
	/**
	 * Возвращает объект доступа к данным
	 * @return 
	 */
	public RandomAccessible getDataReader() {
		return new ByteArrayDataReader(data, getName());
	}
	
	/**
	 * Возвращает поток данных элемента
	 * @return 
	 */
	public ByteArrayInputStream getDataInputStream() {
		return new ByteArrayInputStream(data);
	}
}
