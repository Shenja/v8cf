/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.storage;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;
import ru.shmalevoz.v8cf.IO.FileDataReader;
import ru.shmalevoz.v8cf.IO.RandomAccessible;

/**
 * Файл контейнера cf
 * 
 * @author shmalevoz
 */
public class Container {
	
	private static final Logger log = ru.shmalevoz.utils.Log.getLogger(Container.class.getName());
	
	private static final int SIGN = 0x7FFFFFFF;
	private static final int HEADER_SIZE = 16;
	
	private RandomAccessible source;
	private Page root_page;
	private Entry owner;
	
	private int default_record_size;
	
	/**
	 * Проверяет является ли источник контейнером
	 * @param i
	 * @return 
	 */
	public static boolean hasContainerHeader(byte[] i) {
		return SIGN == ru.shmalevoz.utils.Conversion.ByteArray2Int(i, 0, true);
	}
	
	/**
	 * Проверяет являеся ли источник контейнером
	 * @param i Источник данных
	 * @return 
	 * @throws java.io.IOException 
	 */
	public static boolean hasContainerHeader(RandomAccessible i) throws IOException {
		i.seek(0);
		return hasContainerHeader(i.readByteArray(0, 4));
	}
	
	/**
	 * Чтение контейнера
	 * @param i Источник данных
	 * @param p Владелец контейнера
	 * @throws IOException 
	 */
	private void read(RandomAccessible i, Entry o) throws IOException {
		
		// Проверим сигнатуру
		if (!hasContainerHeader(i)) {
			String err_msg = "Неверный формат хранилища " + i.getName();
			log.severe(err_msg);
			throw new IOException(err_msg);
		}
		
		i.seek(4);
		default_record_size = ru.shmalevoz.utils.Conversion.ByteArray2Int(
			i.readByteArray(0, 4), 0, true);
		log.fine("Размер записи по-умолчанию " + Integer.toHexString(default_record_size));
		
		root_page = new Page(this, HEADER_SIZE);
		owner = o;
	}
	
	/**
	 * Конструктор
	 * 
	 * @param f расположение файла
	 */
	public Container(String f) throws IOException {
		this(new File(f));
	}
		
	/**
	 * Конструктор
	 * 
	 * @param f 
	 */
	public Container(File f) throws IOException {
		this(new FileDataReader(f));
	}
	
	/**
	 * Конструктор
	 * 
	 * @param i 
	 */
	public Container(RandomAccessible i) throws IOException {
		this(i, null);
	}
	
	/**
	 * Конструктор
	 * @param i Источник данных
	 * @param o Владелец контейнера
	 * @throws IOException 
	 */
	public Container(RandomAccessible i, Entry o) throws IOException {
		source = i;
		read(i, o);
	}
	
	/**
	 * Возвращает источник данных
	 * @return 
	 */
	public RandomAccessible getSource() {
		return source;
	}
	
	/**
	 * Возвращает корневую страницу контейнера
	 * 
	 * @return 
	 */
	public Page getRootPage() {
		return root_page;
	}
	
	/**
	 * Возвращает элемент владелец
	 * @return 
	 */
	public Entry getOwnerEntry() {
		return owner;
	}
	
	/**
	 * Возвращает ссылку на родительский элемент
	 * @return 
	 */
	public EntryLink getOwnerEntryLink() {
		return getOwnerEntry() == null ? null : getOwnerEntry().getLink();
	}
}
