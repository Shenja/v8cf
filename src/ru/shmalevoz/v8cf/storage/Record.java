/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.storage;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Logger;
import ru.shmalevoz.v8cf.IO.RandomAccessible;
import ru.shmalevoz.utils.Conversion;

/**
 * Запись в файле cf
 * 
 * @author shmalevoz
 */
public class Record {
	
	private static final Logger log = ru.shmalevoz.utils.Log.getLogger(Record.class.getName());
	
	private static final int HEADER_SIZE = 31;
	private static final int END_ADDR = 0x7FFFFFFF;
	private static final int EMPTY_ADDR = 0x00000000;
	private static final int OFFSET_TOTAL_LENGHT = 2;
	private static final int OFFSET_LENGHT = 11;
	private static final int OFFSET_NEXT_ADDR = 20;
	private static final int ADDR_SIZE = 8;
	
	private final RandomAccessible source;
	
	private final byte[] header = new byte[HEADER_SIZE];
	private byte[] body;
	private int lenght = 0;
	private int next_addr = 0;
	private int rec_addr = 0;
	private int total_lenght = 0;
	
	/**
	 * Возвращает размер записи
	 * 
	 * @return 
	 */
	public int lenght() {
		return lenght;
	}
	
	/**
	 * Проверяет корректность заголовка
	 * 
	 * @param h
	 * @return 
	 */
	public boolean hasValidHeader(byte[] h) {
		
		return h[0]== 0x0D && 
			h[1]== 0x0A &&
			h[10]== 0x20 &&
			h[19]== 0x20 &&
			h[28]== 0x20 &&
			h[29]== 0x0D &&
			h[30]== 0x0A;
	}
	
	/** 
	 * Разбирает данные заголовка
	 * 
	 * @throws IOException 
	 */
	private void parseHeader() throws IOException {
		
		// Проверяем заголовок
		if(!hasValidHeader(header)) {
			String msg = "Неверный заголовок по адресу " + Integer.toHexString(rec_addr);
			log.warning(msg);
			throw new IOException(msg);
		}
		
		// Читаем длину текущей и адрес следующей записи
		next_addr = Conversion.String2Int(
			Arrays.copyOfRange(header, OFFSET_NEXT_ADDR, OFFSET_NEXT_ADDR + ADDR_SIZE), Conversion.HEX);
		int l = Conversion.String2Int(
			Arrays.copyOfRange(header, OFFSET_TOTAL_LENGHT, OFFSET_TOTAL_LENGHT + ADDR_SIZE), Conversion.HEX);
		if (l != 0 && total_lenght == 0) {
			total_lenght = l;
		}
		lenght = Conversion.String2Int(
			Arrays.copyOfRange(header, OFFSET_LENGHT, OFFSET_LENGHT + ADDR_SIZE), Conversion.HEX);
		if (l != 0) {
			lenght = Math.min(l, lenght);
		}
	}
	
	/**
	 * Чтение данных из файла
	 * 
	 * @param f
	 * @param offset
	 * @throws IOException 
	 */
	private void read(int offset) throws IOException {
		
		rec_addr = offset;
		log.info("Чтение записи по адресу " + Integer.toHexString(rec_addr));
		source.seek(offset);
		source.read(header);
		parseHeader();
		
		// Читаем тело записи
		body = new byte[lenght()];
		source.read(body);
		
		log.info("\n\tПолный размер " + Integer.toHexString(total_lenght) + "\n"
			+ "\tРазмер " + Integer.toHexString(lenght) + "\n"
			+ "\tАдрес следующей записи " + Integer.toHexString(next_addr));
	}
	
	/**
	 * Конструктор
	 * 
	 * @param i
	 * @param offset 
	 * @throws java.io.IOException 
	 */
	public Record(RandomAccessible i, int offset) throws IOException {
		source = i;
		read(offset);
	}
	
	/**
	 * Возвращает заголовок записи
	 * @return 
	 */
	public byte[] getHeader() {
		return header;
	}
	
	/**
	 * Возвращает данные записи
	 * @return 
	 */
	public byte[] getBody() {
		return body;
	}
	
	/**
	 * Возвращает адрес записи
	 * @return 
	 */
	public int getAddr() {
		return rec_addr;
	}
	
	/**
	 * Возвращает источник данных
	 * @return 
	 */
	public RandomAccessible getSource() {
		return source;
	}
	
	public boolean hasNext() {
		return next_addr != END_ADDR && next_addr != EMPTY_ADDR;	
	}

	public Record next() {
		
		if (!hasNext()) {
			String err = "Попытка получения записи следующей за последней. Адрес " + Integer.toHexString(rec_addr);
			log.severe(err);
			throw new UnsupportedOperationException(err);
		}
		
		try {
			read(next_addr);
		} catch (Exception e) {
			throw new UnsupportedOperationException(e.getMessage());
		}
		
		return this;
	}
}
