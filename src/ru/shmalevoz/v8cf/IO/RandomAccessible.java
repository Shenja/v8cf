/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.IO;

import java.io.IOException;

/**
 * Интерфейс объекта чтения с произвольным доступом
 * @author shmalevoz
 */
public interface RandomAccessible {
	
	/**
	 * Позиционирование на позицию данных
	 * 
	 * @param pos
	 * @throws IOException 
	 */
	public void seek(int pos) throws IOException;
	
	/**
	 * Позиционирование на позицию данных
	 * 
	 * @param pos
	 * @throws IOException 
	 */
	public void seek(long pos) throws IOException;
	
	/**
	 * Чтение buffer.lenght байт из источника
	 * 
	 * @param buffer
	 * @return
	 * @throws IOException 
	 */
	public int read(byte[] buffer) throws IOException;
	
	/**
	 * Чтение len байт по смещению offset в буфер buffer
	 * 
	 * @param buffer
	 * @param offset
	 * @param len
	 * @return Количество прочитанных байт
	 * @throws IOException 
	 */
	public int read(byte[] buffer, int offset, int len) throws IOException;
	
	/**
	 * Считывает массив байт начиная с позиции
	 * @param len
	 * @param offset
	 * @return 
	 * @throws java.io.IOException 
	 */
	public byte[] readByteArray(int offset, int len) throws IOException;
	
	/**
	 * Возвращает имя источника
	 * @return 
	 */
	public String getName();
	
	/**
	 * Возвращает размер доступных данных
	 * @return 
	 * @throws java.io.IOException 
	 */
	public int lenght() throws IOException;
	
	/**
	 * Возвращает признак содержания сжатых данных
	 * @return 
	 */
	public boolean containsCompressedData();
}
