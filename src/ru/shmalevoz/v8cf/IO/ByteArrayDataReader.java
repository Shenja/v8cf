/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.IO;

import java.io.IOException;

/**
 * Файловая обертка чтения из массива байт
 * @author shmalevoz
 */
public class ByteArrayDataReader implements RandomAccessible {

	private final byte[] source;
	private int position;
	private final int lenght;
	private final String name;
	
	/**
	 * Конструктор
	 * @param s Исходный массив 
	 * @param n Имя потока
	 */
	public ByteArrayDataReader(byte[] s, String n) {
		lenght = s.length;
		source = new byte[lenght];
		System.arraycopy(s, 0, source, 0, lenght);
		position = 0;
		name = n;
	}
	
	@Override
	public void seek(int pos) throws IOException {
		if (pos >= lenght) {
			throw  new IOException("Array of bound exception!");
		}
		position = pos;
	}

	@Override
	public void seek(long pos) throws IOException {
		seek((int) pos);
	}

	@Override
	public int read(byte[] buffer) throws IOException {
		return read(buffer, 0, buffer.length);
	}

	@Override
	public int read(byte[] buffer, int offset, int len) throws IOException {
		
		if (len > buffer.length - offset) {
			throw new IOException("Array of bound exception!");
		}
		
		System.arraycopy(source, position, buffer, offset, len);
		position += len;
		
		return len;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public byte[] readByteArray(int offset, int len) throws IOException {
		byte[] o = new byte[len];
		read(o, offset, len);
		return o;
	}

	@Override
	public int lenght() throws IOException {
		return source.length;
	}

	@Override
	public boolean containsCompressedData() {
		return false;
	}
}
