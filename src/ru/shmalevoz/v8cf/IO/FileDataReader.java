/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.IO;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.File;
import java.util.logging.Logger;

/**
 * Обертка над произвольным чтением из файла
 * @author shmalevoz
 */
public class FileDataReader implements RandomAccessible {
	
	private static final Logger log = ru.shmalevoz.utils.Log.getLogger(FileDataReader.class.getName());
	
	private RandomAccessFile source;
	private String name;

	/**
	 * Конструктор
	 * @param f  Имя файла
	 * @throws java.io.IOException 
	 */
	public FileDataReader(String f) throws IOException {
		this(new File(f));
	}
	
	/**
	 * Конструктор
	 * 
	 * @param f Исходный файл
	 * @throws IOException 
	 */
	public FileDataReader(File f) throws IOException {
		
		name = f.getName();
		
		// Проверяем доступность файла
		String err_msg = "";
		if (!f.exists()) {
			err_msg = "Не найден файл " + name;
		} else if (!f.canRead()) {
			err_msg = "Невозможно прочитать файл " + name;
		}
		if (!err_msg.isEmpty()) {
			log.severe(err_msg);
			throw new IOException(err_msg);
		}
		
		source = new RandomAccessFile(f, "r");
	}
	
	@Override
	public void seek(int pos) throws IOException {
		seek((long) pos);
	}

	@Override
	public void seek(long pos) throws IOException {
		source.seek(pos);
	}

	@Override
	public int read(byte[] buffer) throws IOException {
		return source.read(buffer);
	}

	@Override
	public int read(byte[] buffer, int offset, int len) throws IOException {
		return source.read(buffer, offset, len);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public byte[] readByteArray(int offset, int len) throws IOException {
		byte[] o = new byte[len];
		read(o, offset, len);
		return o;
	}

	@Override
	public int lenght() throws IOException {
		return (int) source.length();
	}

	@Override
	public boolean containsCompressedData() {
		return true;
	}

}
